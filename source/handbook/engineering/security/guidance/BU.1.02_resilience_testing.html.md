---
layout: markdown_page
title: "BU.1.02 - Resilience Testing Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# BU.1.02 - Resilience Testing

## Control Statement

GitLab performs backup restoration and/or failover tests quarterly to confirm the reliability and integrity of system backups and/or recovery operations.

## Context

By validating system backups/recovery operations in the event of an actual disaster or other disruption to service; we will have greater proficiency in restoring service to customers.

## Scope

TBD

## Ownership

Control Owner:

 * Infrastructure Team

Process owner:

* Infrastructure Team

## Implementation Guidance

For detailed implementation guidance relevant to GitLab team-members, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/BU.1.02_resilience_testing.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/BU.1.02_resilience_testing.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/BU.1.02_resilience_testing.md).

## Framework Mapping

* ISO
  * A.12.3.1
* SOC
  * A1.2
* PCI
  * 12.10.1
