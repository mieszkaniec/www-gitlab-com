---
layout: job_family_page
title: "Sales Development Representative 3"
---
## Job Description

After attaining an Sales Development Representative (SDR) 3 role, you can expect an increase in compensation and a reduction in your Sales targets as the SDR 3 role is a team coach and mentor for more junior members of the team. You will receive substantial Sales training to expand your current knowledge into pricing conversations and negotiations. You will be expected to pass this knowledge along to your team and set them up for success in their individual performance. Upon reaching target goals and acquiring certification on these skills, the next career step is either an SDR Management role or a career branch into another function in GitLab. The next step should be driven by the SDR and Manager together.

### Responsibilities

* Leads territory team role play calls
* Regularly performs call coaching evaluations for SDR team members
* Helps craft Outreach sequences for events/campaigns and outbound. Conduct A/B Testing of sequences to determine effectiveness and share best practices with team.
* Assists new hires as an onboarding buddy and acts as a mentor for new SDR hires in helping them navigate their key accounts.

### Requirements

* Six (6) months in an [SDR 2](/job-families/marketing/sales-development-representative/sdr-2/) role at GitLab with successful quota attainment
* Demonstrates thought leadership and training that benefits the SDR team
* Demonstrates exemplary account strategy/process acumen
* GitLab leadership recommends for promotion
* Demonstrates dedication to continuous improvement by investing time to read, study, and share learnings from books, Udemy classes, Toastmasters, Certifications, Approved Sales Training, CEO Shadow Program, etc.