---
layout: job_family_page
title: "Sales Development Representative 2"
---
## Job Description

After attaining an Sales Development Representative (SDR) 2 role, you can expect an increase in your compensation and your pipeline generating targets. You will also discuss your ideal career path with your manager and set action steps to achieve it. Topics of discussion should consist of functionalities of interest and steps to be taken by both SDR and Manager to get there. For a minimum of 6 months in the SDR 2 role, you will continue improving your Sales skills while also aiding in the development of these skills with other members of your teams through training and coaching. Upon reaching target goals and acquiring certification on these skills, the next step is an [SDR 3](/job-families/marketing/sales-development-representative/sdr-3) role.  All the while the SDR and Manager should have clear conversations and setting milestones around career progression at GitLab.

### Responsibilities

* Attends GitLab sponsored events
* Participates and owns projects benefiting the SDR organization
* Assists with campaigns - lead pulls and feedback 
* Attend field marketing events to engage with participants identify opportunities, and to schedule meetings 
* Work in collaboration with Digital Marketing to develop targeted marketing tactics against your assigned target accounts

### Requirements

* 1-2 years proven [SDR](/job-families/marketing/sales-development-representative) experience (Either at GitLab or a past role)
* Consistently hits/exceed quota
* Will present at an SDR Quarterly Business Review (QBR) or all hands
* Networked at local DevOps events (non-GitLab) 
* Understands, executes and presents an Account Based Marketing (ABM) approach

